package com.example.unnom.doyourbankaccount;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by unnom on 31/10/16.
 */
public class Depense extends AppCompatActivity {
    private EditText intitule ;
    private EditText somme;
    private Button validate;

    protected void onCreate(Bundle save){
        super.onCreate(save);
        setContentView(R.layout.add_depense);
        intitule = (EditText)findViewById(R.id.intitule);
        somme  =(EditText)findViewById(R.id.somme);
        validate = (Button)findViewById(R.id.validateSomme);
        Bundle tmp = getIntent().getExtras();
        final String tmpPath = tmp.getString("path");
        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newIntitule = intitule.getText().toString();

                String newSomme = somme.getText().toString();
                if(!Error.emptyField(somme) && !Error.isNotValide(somme) && !Error.emptyField(intitule)) {
                    addDepense(newIntitule, newSomme, tmpPath);
                    finish();
                }
            }
        });

    }

    private void addDepense(String nom, String somme, String path){
        float soldes = Float.parseFloat(somme);
        soldes = (soldes*100);
        int tmp = (int)soldes;
        soldes = tmp/100.0f;
        somme = Float.toString(soldes);

        String add = nom.substring(0,1 ).toUpperCase() + nom.substring(1) +";"+somme+";";
        try {
            FileOutputStream file = openFileOutput(path, MODE_APPEND);
            file.write(add.getBytes());
            file.close();
        }catch(IOException e){
            Toast toast = Toast.makeText(this, path +"n'as pas été trouvé", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
