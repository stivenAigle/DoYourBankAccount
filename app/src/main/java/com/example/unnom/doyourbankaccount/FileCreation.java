package com.example.unnom.doyourbankaccount;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.*;

/**
 * Created by unnom on 27/10/16.
 */
public class FileCreation extends AppCompatActivity {
    private EditText date ;
    private EditText solde;
    private Button validate;

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_file);
        date = (EditText)findViewById(R.id.addDate);
        solde = (EditText)findViewById(R.id.addSolde);
        validate = (Button)findViewById(R.id.valdate);

        validate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String test = solde.getText().toString();
                if(!Error.emptyField(solde) && !Error.isNotValide(solde) && !Error.emptyField(date)) {
                    createFile(date.getText().toString(), Float.parseFloat(solde.getText().toString()));
                    finish();
                }

            }
        });

    }
    private void createFile(String date, float soldes){
        FileOutputStream output;
        soldes = (soldes*100);
        int tmp = (int)soldes;
        soldes = tmp/100.0f;
        try {
            date = date.substring(0, 1).toUpperCase() + date.substring(1);
            output = openFileOutput(date, MODE_PRIVATE);
            String data =date+";"+Float.toString(soldes)+";";
            output.write(data.getBytes());


        }catch(IOException e){
            Toast toast = Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
            toast.show();

        }




    }

}
