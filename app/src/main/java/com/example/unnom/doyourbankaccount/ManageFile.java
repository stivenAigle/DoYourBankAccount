package com.example.unnom.doyourbankaccount;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.*;

import java.io.FileInputStream;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.exit;

/**
 * Created by unnom on 27/10/16.
 */
public class ManageFile {

    private ListView list;
    private SimpleAdapter simpleAdpater;
    private List<HashMap<String, String>> fileList;
    public final static String pathFile = "listeFichier";
    private String keyDate = "date";
    private String keySolde = "solde";
    private Context context;

    ManageFile(final Context context) {
        this.context = context;
        list = new ListView(context);
        fileList = new ArrayList<>();

        simpleAdpater = new SimpleAdapter(context, fileList, android.R.layout.simple_list_item_2, new String[]{keyDate, keySolde}, new int[]{android.R.id.text1, android.R.id.text2});
        initFile();
        list.setAdapter(simpleAdpater);
        list.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(context, File.class);
                intent.putExtra("path", ((HashMap<String, String>) (adapterView).getItemAtPosition(position)).get(keyDate));
                context.startActivity(intent);


            }
        });


    }

    public void initFile() {
        fileList.clear();
        simpleAdpater.notifyDataSetChanged();
        //  try {
        //AssetManager assetManager = context.getAssets();
          /*  FileInputStream input = context.openFileInput(pathFile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));*/
        String read;
        String[] files = context.fileList();
           /* while ((read = bufferedReader.readLine()) != null) {
                addFile(read);
                *//*Toast toast = Toast.makeText(context, read, Toast.LENGTH_SHORT );
                toast.show();
*//*

            }*/
        for (String f : files) {
            addFile(f);
        }
       /* } catch (IOException e) {
            Toast toast = Toast.makeText(context, "il n'y a aucun fichier", Toast.LENGTH_SHORT);
            toast.show();

        }*/

    }

    private void addFile(String path) {
        if (path.equals("instant-run")) {
            return;
        }
        try {
//            Scanner scanner = new Scanner(assetManager.open(path));
            FileInputStream input = context.openFileInput(path);
            Scanner scanner = new Scanner(input);
            scanner.useDelimiter(";");
            HashMap<String, String> hash = new HashMap<>();
            String date;
            date = scanner.next();
            String soldes = scanner.next();
            int i = 0;
            float somme = Float.parseFloat(soldes);
            while (scanner.hasNext()) {
                String temp = scanner.next();
                if ((i % 2) == 1) {
                    float add = Float.parseFloat(temp);
                    somme -= add;

                }
                i++;
            }
            somme = (somme*100);
            int tmp = (int)somme;
            somme = tmp/100.0f;

            hash.put(keyDate, date);
            hash.put(keySolde, Float.toString(somme));
            fileList.add(hash);
            simpleAdpater.notifyDataSetChanged();
            input.close();

        } catch (IOException e) {
            Toast toast = Toast.makeText(context, "Fichier non trouvé", Toast.LENGTH_SHORT);
            toast.show();

        }
    }

    public ListView getList() {
        return list;
    }
}
