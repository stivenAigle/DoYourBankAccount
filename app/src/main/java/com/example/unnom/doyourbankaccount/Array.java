package com.example.unnom.doyourbankaccount;

import java.util.ArrayList;

/**
 * Created by unnom on 30/10/16.
 */
public class Array<E> extends ArrayList<E> {

    public E getLast(){
        return get(size()-1);
    }

    public E getFirst(){
        return get(0);
    }


}
